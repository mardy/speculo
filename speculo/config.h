/* vi: set et sw=4 ts=4 cino=t0,(0: */
/*
 * Copyright (C) 2013 Alberto Mardegan <info@mardy.it>
 *
 * This file is part of libspeculo.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 */

#ifndef SPECULO_CONFIG_H
#define SPECULO_CONFIG_H

#define SPECULO_AREA_MAX_NAME_LENGTH    31
#define SPECULO_AREA_DEFAULT_MAX_SIZE     (16 * 1024 * 1024)
#define SPECULO_AREA_DEFAULT_MAX_TRASH_PERCENTAGE 40
#define SPECULO_AREA_DEFAULT_COMPACT_SIZE     (64 * 1024)

#define USE_MADVISE 1

#ifdef __GNUC__
#  if __GNUC__ * 100 + __GNUC_MINOR__ >= 407
#    define ATOMIC_STORE(ptr, value) \
        __atomic_store_n(ptr, value, __ATOMIC_RELEASE)
#    define ATOMIC_LOAD(ptr) \
        __atomic_load_n(ptr, __ATOMIC_ACQUIRE)
#  else
#    define ATOMIC_STORE(ptr, value) \
        do { \
            __sync_synchronize(); \
            *ptr = value; \
        } while (0)
static inline int atomic_load(const void *ptr)
{
    int value = *((int *)ptr);
    __sync_synchronize();
    return value;
}
#    define ATOMIC_LOAD(ptr) atomic_load(ptr)
#  endif
#else
#  error "The ATOMIC_STORE macro is not implemented for your compiler"
#endif

#endif /* SPECULO_CONFIG_H */
