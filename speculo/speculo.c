/* vi: set et sw=4 ts=4 cino=t0,(0: */
/*
 * Copyright (C) 2013 Alberto Mardegan <info@mardy.it>
 *
 * This file is part of libspeculo.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 */

#include "config.h"
#include "speculo.h"

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <sched.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/queue.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define MAX_SHMEM_NAME_LENGTH       (SPECULO_AREA_MAX_NAME_LENGTH + 10)
/* The path length of the manifest file depends on the value of the TMPDIR
 * environment variable, which cannot be predicted in advance. So, use
 * _POSIX_PATH_MAX. */
#define MAX_MANIFEST_PATH_LENGTH       _POSIX_PATH_MAX

#define AREA_INITIAL_SIZE   (sizeof(speculo_area_header) + \
                             sizeof(speculo_chunk_header))

#ifdef P_tmpdir
#define SPECULO_MANIFEST_DEFAULT_DIR    P_tmpdir
#else
#define SPECULO_MANIFEST_DEFAULT_DIR    "/tmp"
#endif

#define AREA_HEADER(area) ((speculo_area_header *)area->base_address)
#define CHUNK_HEADER(chunk) \
    ((speculo_chunk_header *) \
     (chunk->area->base_address + \
      chunk->base_offset))
#define CHUNK_OFFSET(chunk_header, area_header) \
    ((char *)chunk_header - (char *)area_header)

#ifdef __GNUC__
#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)
#else
#define likely(x)       (x)
#define unlikely(x)     (x)
#endif

typedef struct speculo_obsoleted_area_t {
    char *base_address;
    size_t mapped_size;
    int held_chunks;
    LIST_ENTRY(speculo_obsoleted_area_t) list;
} speculo_obsoleted_area;

LIST_HEAD(speculo_obsoleted_areas_head_t, speculo_obsoleted_area_t);

struct speculo_area_t {
    char name[SPECULO_AREA_MAX_NAME_LENGTH + 1];
    char *base_address;
    enum access_type_t {
        AREA_READER = 0,
        AREA_WRITER
    } access_type;
    union {
        struct reader_data_t {
            size_t mapped_size;
            int held_chunks;
            struct speculo_obsoleted_areas_head_t obsoleted_areas;
        } r;
        struct writer_data_t {
            size_t max_size;
            size_t compact_size;
            int flags;
            int max_trash_percentage;
            char shm_suffix;
        } w;
    } x;
};

typedef struct speculo_area_header_t {
    enum area_state_t {
        AREA_STATE_INITIALIZING = 0,
        AREA_STATE_READY,
        AREA_STATE_OBSOLETED,
        AREA_STATE_LAST,
    } state;
    size_t discarded_size;
    size_t expired_size;
    size_t total_size; /* includes the header itself, and all chunks;
                          "area->base_offset + total_size" gives the first
                          unused byte. */
    struct timespec next_expiration_time;

    /* ID of the shmem area; this is incremented every time that the SHM
     * object gets obsoleted and replaced by a new one.  This field
     * serves to tell the readers whether their chunk handles are still
     * valid.
     */
    int id;

    /* ID of the last allocated chunk. */
    int last_id;
} speculo_area_header;

typedef struct speculo_chunk_header_t {
    size_t size;
    size_t offset_to_obsoleted;
    enum state_t {
        CHUNK_UNALLOCATED = 0,
        CHUNK_ALLOCATED,
        CHUNK_WRITTEN,
        CHUNK_OBSOLETED,
        CHUNK_EXPIRED,
    } state;
    int id;             /* unique id */
    int update_id;      /* retained during updates */
    struct timespec expiration_time;
} speculo_chunk_header;

typedef char shm_name_buffer[MAX_SHMEM_NAME_LENGTH];

static int update_area_manifest(speculo_area *area, char suffix);

static int initialized = 0;
static long page_size = 0;
static const char *tmpdir = NULL;

/*!
 * \file
 * \author Alberto Mardegan <mardy@users.sourceforge.net>
 * \copyright GNU Lesser Public Licence v2.1
 */

/*!
 * \mainpage
 *
 * \tableofcontents
 *
 * \section about About speculo
 *
 * \c speculo is a tiny library meant to facilitate the usage of shared memory
 * in POSIX systems. It allows developers to easily implement a design where
 * one writer process shares chunks of data with many reader processes, with
 * the guarantee that at any point in time readers have a consistent view of
 * the data, and without the need of locks. It's a thin layer on top of the
 * POSIX shm_open()/mmap() APIs, and as such it should preserve all the speed
 * of SHM while hiding some of the shortcomings.
 *
 * \c speculo in a few bullet points:
 * \li small \c C library for IPC over shared memory
 * \li good test coverage
 * \li one writer, many readers (per \c speculo_area)
 * \li zerocopy
 * \li data is written and read in chunks of arbitrary size
 * \li a data chunk becomes visible to the readers as soon as the writer commits it
 * \li data chunks can have an expiration time
 * \li data chunks can be obsoleted by a newer copy
 * \li with the exception of a few checkpoints guarded by memory barriers,
 * bytes written to the SHM object won't ever change their value
 * \li when needed, the memory area is copied into a new SHM object with
 * expired and obsoleted chunks removed to make room for new data
 *
 * \section usecases Use cases
 *
 * \c speculo has been designed to mainly cover these two use-cases:
 * \li sharing a stream of data packets
 * \li sharing one or more data blocks whose value can change over time
 *
 * \subsection speculo_as_stream Sharing a stream of data packets
 *
 * This use-case can be implemented by allocating and writing chunks of data
 * into a SHM object (which we call \c speculo_area) and setting an expiration
 * time on each of them: once the SHM object is full (or a certain percentage
 * of expired chunks has been reached) the data chunks which are still valid
 * get copied over into a new SHM file, while the expired ones are discarded.
 *
 * \subsection speculo_for_data_blocks Sharing a changeable data block
 *
 * Any data chunk written in a SHM object can be updated with new data: this is
 * implemented in speculo by marking the old chunk as obsoleted and adding a
 * new chunk with the new data, and remembering the association between the two.
 *
 * \section getinvolved Get involved
 *
 * The source code of speculo is hosted at <a
 * href="https://gitlab.com/mardy/speculo">GitLab</a>. There one can also find
 * the issue tracker.
 *
 * Discussions about the usage of speculo and its development should primarily
 * take place in the <a
 * href="http://lists.mardy.it/listinfo.cgi/speculo-mardy.it">speculo mailing
 * list</a>.
 * The main developer can also be reached in IRC: \a mardy @ \a freenode.net
 *
 * \author Alberto Mardegan <mardy@users.sourceforge.net>
 * \copyright GNU Lesser Public Licence v2.1
 */

static void speculo_initialize()
{
    if (initialized) return;

    page_size = sysconf(_SC_PAGESIZE);
    tmpdir = getenv("TMPDIR");
    if (tmpdir == NULL) {
        tmpdir = SPECULO_MANIFEST_DEFAULT_DIR;
    }

    initialized = 1;
}

static int manifest_path_from_area_name(const char *area_name,
                                        char *manifest, size_t size)
{
    return snprintf(manifest, size, "%s/spcl-%s", tmpdir, area_name);
}

static int shmem_name_from_area_name(const char *area_name,
                                     char *shmem_name, size_t size)
{
    return snprintf(shmem_name, size, "/Spcl-%s.0", area_name);
}

static void get_clock(struct timespec *ts)
{
#ifdef _POSIX_MONOTONIC_CLOCK
    clock_gettime(CLOCK_MONOTONIC, ts);
#elif (defined(_POSIX_TIMERS) && _POSIX_TIMERS > 0)
    clock_gettime(CLOCK_REALTIME, ts);
#else
    ts->tv_sec = time(0);
    ts->tv_nsec = 0;
#endif
}

/*
 * Compare times; if a time has tv_sec < 0, then it's considered to be
 * infinite.
 *
 * Returns: < 0 if  t0 < t1
 *          0   if  t0 == t1
 *          > 0 if  t0 > t1
 */
static int clock_compare(const struct timespec *t0,
                         const struct timespec *t1)
{
    if (t0->tv_sec < 0) return 1;
    if (t1->tv_sec < 0) return -1;

    int diff = t0->tv_sec - t1->tv_sec;
    if (diff != 0) return diff;
    return t0->tv_nsec - t1->tv_nsec;
}

/* Return the smallest value >= size which is 8-byte aligned. */
static inline size_t align_size(size_t size)
{
    return (size + 7) & ~0x7;
}

static inline void init_sentinel_chunk(speculo_area_header *area_header)
{
    /* The area should always end with an unitialized speculo_chunk_header
     * having its state field set to CHUNK_UNALLOCATED. This is needed in order
     * to mark the end of the area. */
    speculo_chunk_header *last;

    last = (speculo_chunk_header*)
        ((char *)area_header +
         area_header->total_size - sizeof(speculo_chunk_header));
    last->id = 0;
    last->state = CHUNK_UNALLOCATED;
}

static int area_map_create(speculo_area *area)
{
    char shmem_name[MAX_SHMEM_NAME_LENGTH];
    speculo_area_header *area_header;
    int ret;
    int fd;

    assert(area->base_address == NULL);

    shmem_name_from_area_name(area->name, shmem_name, sizeof(shmem_name));
    fd = shm_open(shmem_name,
                  O_CREAT | O_RDWR | O_TRUNC,
                  S_IWUSR | S_IRUSR | S_IRGRP);
    if (unlikely(fd < 0)) return -1;

    ret = ftruncate(fd, area->x.w.max_size);
    if (unlikely(ret < 0)) {
        shm_unlink(shmem_name);
        return -1;
    }

    area->base_address = mmap(NULL,
                              area->x.w.max_size,
                              PROT_READ | PROT_WRITE,
                              MAP_SHARED,
                              fd,
                              0);
    close(fd);
    if (unlikely(area->base_address == NULL)) {
        shm_unlink(shmem_name);
        return -1;
    }
#if USE_MADVISE
    posix_madvise(area->base_address, area->x.w.max_size, POSIX_MADV_SEQUENTIAL);
#endif

    area_header = AREA_HEADER(area);
    area_header->state = AREA_STATE_INITIALIZING;
    area_header->discarded_size = 0;
    area_header->expired_size = 0;
    area_header->total_size = AREA_INITIAL_SIZE;
    init_sentinel_chunk(area_header);
    area_header->next_expiration_time.tv_sec = -1;
    area_header->id = 1;
    area_header->last_id = 1;
    ATOMIC_STORE(&area_header->state, AREA_STATE_READY);

    area->x.w.shm_suffix = '0';

    return 0;
}

/* Attempt to open a new SHM object, using the names
 *   <area-name>.0, <area-name>.1, etc.
 *
 * Return the newly opened fd, and the file name last character in
 * "suffix_ptr".
 */
static int area_open_new_shm(speculo_area *area, shm_name_buffer new_name,
                             char *suffix_ptr)
{
    char *suffix;
    int length;
    int fd;
    int i;

    length = shmem_name_from_area_name(area->name, new_name,
                                       sizeof(shm_name_buffer));
    suffix = (char *)new_name + length - 1;

    /* Avoid starting always with the '0' suffix, because that would mean
     * always swapping between '0' and '1'. This would be risky because if a
     * fast writer triggers two area compactations right after a reader has
     * just read the manifest file, the reader could end up reading the newest
     * writer area before it is actually ready. */
    i = (*suffix_ptr - '0') % 20;
    do {
        *suffix = '0' + i;
        i++;
        fd = shm_open(new_name,
                      O_CREAT | O_RDWR | O_EXCL,
                      S_IWUSR | S_IRUSR | S_IRGRP);
        if (likely(fd > 0)) {
            *suffix_ptr = *suffix;
            return fd;
        }
        if (unlikely(errno != EEXIST)) return -1;

        /* We can't iterate forever: we must make sure that '0' + i fits in a
         * byte.
         */
    } while (i + '0' < 255);
    return -1;
}

/*
 * Get next chunk, or the first one if "chunk_header" is NULL.
 * This function doesn't skip over deleted or obsoleted chunks.
 *
 * Returns NULL if there are no more chunks.
 */
static speculo_chunk_header *
area_get_next_chunk_header(const speculo_area_header *area_header,
                           const speculo_chunk_header *chunk_header,
                           enum state_t *chunk_state)
{
    speculo_chunk_header *next;
    size_t chunk_end;
    enum state_t state;

    chunk_end = (chunk_header == NULL) ?
        sizeof(speculo_area_header) :
        ((char *)chunk_header - (char *)area_header) +
        sizeof(speculo_chunk_header) + align_size(chunk_header->size);

    next = (speculo_chunk_header*)((char *)area_header + chunk_end);
    state = ATOMIC_LOAD(&next->state);
    if (state == CHUNK_UNALLOCATED) return NULL;

    if (chunk_state != 0) *chunk_state = state;
    return next;
}

static void area_update_expired_size(speculo_area_header *header,
                                     const struct timespec *now)
{
    speculo_chunk_header *chunk = NULL;
    struct timespec next_expiration_time;
    size_t expired_size;
    enum state_t state;

    expired_size = 0;
    next_expiration_time.tv_sec = -1;

    while ((chunk = area_get_next_chunk_header(header, chunk,
                                               &state)) != NULL) {
        /* skip chunks which are not valid */
        if (state != CHUNK_WRITTEN &&
            state != CHUNK_ALLOCATED)
            continue;

        /* Is the chunk expired? */
        if (clock_compare(now, &chunk->expiration_time) >= 0) {
            size_t chunk_aligned_size = align_size(chunk->size);
            chunk->state = CHUNK_EXPIRED;
            expired_size += chunk_aligned_size;
        } else {
            /* check if the chunk will be the first one to expire */
            if (clock_compare(&chunk->expiration_time,
                              &next_expiration_time) < 0) {
                next_expiration_time = chunk->expiration_time;
            }
        }
    }
    header->expired_size += expired_size;
    header->next_expiration_time = next_expiration_time;
}

/*
 * Copy the valid contents from "header" into "base_address".
 * This function assumes that "base_address" is large enough to fit all the
 * data.
 */
static void area_copy(const speculo_area_header *header, void *base_address,
                      const struct timespec *now)
{
    speculo_area_header *new_header = base_address;
    speculo_chunk_header *chunk = NULL;
    size_t chunk_total_size;
    enum state_t state;
    char *area_end;

    new_header->discarded_size = 0;
    new_header->total_size = sizeof(speculo_area_header);
    new_header->id = header->id + 1;
    new_header->last_id = header->last_id;
    new_header->next_expiration_time.tv_sec = -1;
    area_end = (char *)new_header + sizeof(speculo_area_header);

    while ((chunk = area_get_next_chunk_header(header, chunk,
                                               &state)) != NULL) {
        /* skip chunks which are not valid */
        if (state != CHUNK_WRITTEN &&
            state != CHUNK_ALLOCATED)
            continue;

        /* skip expired chunks */
        if (clock_compare(now, &chunk->expiration_time) >= 0)
            continue;

        /* check if the chunk will be the first one to expire */
        if (clock_compare(&chunk->expiration_time,
                          &new_header->next_expiration_time) < 0) {
            new_header->next_expiration_time = chunk->expiration_time;
        }

        /* copy the chunk to the new area */
        chunk_total_size =
            sizeof(speculo_chunk_header) + align_size(chunk->size);
        memcpy(area_end, chunk, chunk_total_size);
        area_end += chunk_total_size;
    }
    new_header->total_size = area_end - (char *)new_header +
        sizeof(speculo_chunk_header);
    init_sentinel_chunk(new_header);
    ATOMIC_STORE(&new_header->state, AREA_STATE_READY);
}

static int update_area_manifest(speculo_area *area, char suffix)
{
    char manifest_path_tmp[MAX_MANIFEST_PATH_LENGTH + 2];
    char manifest_path[MAX_MANIFEST_PATH_LENGTH];
    int manifest;
    int written;

    manifest_path_from_area_name(area->name, manifest_path,
                                 sizeof(manifest_path));
    sprintf(manifest_path_tmp, "%s.0", manifest_path);

    manifest = open(manifest_path_tmp,
                    O_WRONLY | O_CREAT | O_TRUNC,
                    S_IRUSR | S_IWUSR | S_IRGRP);
    if (unlikely(manifest == -1)) return -1;

    written = write(manifest, &suffix, 1);

    close(manifest);
    rename(manifest_path_tmp, manifest_path);
    return written == 1 ? 0 : -1;
}

/*
 * Try to compact the SHM area by copying the valid chunks into a new SHM
 * object.
 * Returns >= 0 if compacting took place.
 */
static inline int area_try_compact(speculo_area *area, int force_compact)
{
    char shmem_name[MAX_SHMEM_NAME_LENGTH];
    char suffix;
    struct timespec now;
    speculo_area_header *header;
    char *base_address;
    int max_trash_percentage;
    int fd;
    int ret;

    assert(area->base_address != NULL);
    header = AREA_HEADER(area);

    /* If we haven't reached the minimum compacting size, do nothing */
    if (header->total_size < area->x.w.compact_size && !force_compact)
        return -1;

    /* If there are no deleted or expired chunks, compacting won't bring any
     * benefit: return. */
    get_clock(&now);
    if (header->discarded_size + header->expired_size <= 0 &&
        clock_compare(&now, &header->next_expiration_time) < 0)
        return -1;

    /* Actually measure how much memory could be discarded; maybe the amount
     * isn't worth the effort */
    area_update_expired_size(header, &now);

    /* Is there enough trash to justify a compact operation? */
    max_trash_percentage =
        force_compact ? 0 : area->x.w.max_trash_percentage;
    if (header->discarded_size + header->expired_size <=
        max_trash_percentage * header->total_size / 100)
        return -1;

    /* Get a new SHM object, and copy the valid contents into it */
    suffix = area->x.w.shm_suffix;
    fd = area_open_new_shm(area, shmem_name, &suffix);
    if (unlikely(fd < 0)) return -1;

    ret = ftruncate(fd, area->x.w.max_size);
    if (unlikely(ret < 0)) goto error_delete_shm;

    base_address = mmap(NULL, area->x.w.max_size,
                        PROT_READ | PROT_WRITE,
                        MAP_SHARED, fd, 0);
    close(fd);
    if (unlikely(base_address == NULL)) goto error_delete_shm;
#if USE_MADVISE
    posix_madvise(base_address, area->x.w.max_size, POSIX_MADV_SEQUENTIAL);
#endif

    area_copy(header, base_address, &now);

    ret = update_area_manifest(area, suffix);
    if (unlikely(ret < 0)) goto error_unmap_shm;

    ATOMIC_STORE(&header->state, AREA_STATE_OBSOLETED);
    munmap(area->base_address, area->x.w.max_size);
    /* destroy the previous SHM area; the suffix is in area->x.w.shm_suffix */
    shmem_name[strlen(shmem_name) - 1] = area->x.w.shm_suffix;
    shm_unlink(shmem_name);

    /* update the local structure */
    area->x.w.shm_suffix = suffix;
    area->base_address = base_address;

    return 0;

error_unmap_shm:
    munmap(base_address, area->x.w.max_size);
error_delete_shm:
    shm_unlink(shmem_name);
    return -1;
}

/*!
 * Creates a new \a speculo_area object. Note that this doesn't actually create
 * the shared memory area, which will be allocated only when
 * speculo_area_create() is called.
 * \param name the name of the area (no more than SPECULO_AREA_MAX_NAME_LENGTH
 * characters).
 * \param flags \c 0 or a combination of the supported flags:
 * \li \c SPECULO_MODE_EXCLUSIVE if the area already exists,
 * speculo_area_create() will fail (normally, it would just overwrite it)
 *
 * \return a pointer to a \c speculo_area structure.
 *
 * \sa speculo_area_create
 */
speculo_area *speculo_area_new(const char *name, int flags)
{
    speculo_area *area;

    speculo_initialize();

    area = malloc(sizeof(speculo_area));
    if (unlikely(area == NULL)) return NULL;

    strncpy(area->name, name, SPECULO_AREA_MAX_NAME_LENGTH + 1);
    area->name[SPECULO_AREA_MAX_NAME_LENGTH] = '\0';
    area->base_address = NULL;
    area->x.w.flags = flags;
    area->x.w.max_size = SPECULO_AREA_DEFAULT_MAX_SIZE;
    area->x.w.compact_size = SPECULO_AREA_DEFAULT_COMPACT_SIZE;
    area->x.w.max_trash_percentage = SPECULO_AREA_DEFAULT_MAX_TRASH_PERCENTAGE;
    return area;
}

/*!
 * Creates the shared memory area. When this function completes, the shared
 * memory area object will have become visible to other speculo clients.
 * \param area pointer to a \c speculo_area.
 *
 * \return \c 0 if successful, -1 on error.
 */
int speculo_area_create(speculo_area *area)
{
    char manifest_path[MAX_MANIFEST_PATH_LENGTH];
    int ret;

    assert(area != NULL);

    manifest_path_from_area_name(area->name, manifest_path, sizeof(manifest_path));

    /* If exclusive access was requested and the manifest file exists, then
     * fail. */
    if (area->x.w.flags & SPECULO_MODE_EXCLUSIVE &&
        access(manifest_path, F_OK) == 0) {
        return -1;
    }

    ret = area_map_create(area);
    if (unlikely(ret < 0)) return ret;

    ret = update_area_manifest(area, area->x.w.shm_suffix);
    return ret;
}

/*!
 * Sets the maximum file size of the memory area. Given that the memory is not
 * allocated until it's actually taken into use, it's safe to set the maximum
 * size to a very large number.
 * \param area pointer to a \c speculo_area.
 * \param size maximum size of the shared memory area.
 * \attention This function must be called before calling speculo_area_create()
 */
int speculo_area_set_max_size(speculo_area *area, size_t size)
{
    assert(area != NULL);
    assert(size > 0);

    area->x.w.max_size = size;
    if (unlikely(area->base_address != NULL))
        return -1;
    return 0;
}

/*!
 * Sets the maximum percentage of deleted and expired chunks; if a new
 * allocation is requested when the size occupied by deleted and expired chunks
 * is greater than the given percentage of the total area size, the area might
 * be recompacted.
 * \param area pointer to a \c speculo_area.
 * \param percentage maximum percentage of deleted and expired chunks.
 * \attention This function must be called before calling speculo_area_create()
 */
void speculo_area_set_max_trash_percentage(speculo_area *area,
                                           int percentage)
{
    assert(area != NULL);
    assert(percentage > 0 && percentage <= 100);
    area->x.w.max_trash_percentage = percentage;
}

/*!
 * Sets the minimum size after which compacting might happen. As long as the
 * used area size is less then the given size, no compacting will occur.
 * \param area pointer to a \c speculo_area.
 * \param size minimum size of the shared area.
 * \attention This function must be called before calling speculo_area_create()
 */
void speculo_area_set_min_compact_size(speculo_area *area, size_t size)
{
    assert(area != NULL);
    area->x.w.compact_size = size;
}

/*!
 * Initializes a \c speculo_chunk structure. The \a chunk structure can then be
 * used to allocate some memory in the shared area.
 * \param area pointer to a \c speculo_area.
 * \param chunk pointer to an uninitialized \c speculo_chunk structure.
 * \sa speculo_chunk_allocate
 */
int speculo_area_create_chunk(speculo_area *area, speculo_chunk *chunk)
{
    assert(area != NULL);
    assert(chunk != NULL);
    chunk->area = area;
    chunk->id = -1;
    chunk->update_id = -1;
    chunk->base_offset = 0;
    return 0;
}

/*!
 * Allocates \a size bytes of shared memory and returns a pointer to it. After
 * the memory has been allocated, the client can write to it and must call
 * speculo_chunk_commit() when done.
 * \param chunk pointer to an initialized \c speculo_chunk.
 * \param size the number of bytes to allocate.
 * \return a pointer to the newly allocated memory, or \c NULL if an error occurred.
 * \sa speculo_area_create_chunk, speculo_chunk_commit
 */
void *speculo_chunk_allocate(speculo_chunk *chunk, size_t size)
{
    speculo_area *area;
    speculo_area_header *area_header;
    speculo_chunk_header *chunk_header;
    size_t needed_size;
    int force_compact;

    assert(chunk != NULL);
    assert(chunk->base_offset == 0);

    area = chunk->area;
    assert(area->base_address != NULL);

    needed_size = align_size(size) + sizeof(speculo_chunk_header);
    force_compact =
        (AREA_HEADER(area)->total_size + needed_size > area->x.w.max_size);
    area_try_compact(area, force_compact);

    area_header = AREA_HEADER(area);
    if (unlikely(area_header->total_size + needed_size > area->x.w.max_size))
        return NULL;

    chunk->base_offset = area_header->total_size - sizeof(speculo_chunk_header);
    chunk->id = area_header->last_id++;
    if (chunk->update_id == -1)
        chunk->update_id = chunk->id;
    chunk->area_id = area_header->id;
    chunk_header = CHUNK_HEADER(chunk);
    chunk_header->size = size;
    chunk_header->offset_to_obsoleted = 0;
    chunk_header->id = chunk->id;
    chunk_header->update_id = chunk->update_id;
    chunk_header->expiration_time.tv_sec = -1;
    area_header->total_size = area_header->total_size + needed_size;
    init_sentinel_chunk(area_header);
    ATOMIC_STORE(&chunk_header->state, CHUNK_ALLOCATED);

    return ((char *)chunk_header + sizeof(speculo_chunk_header));
}

/*!
 * Sets the expiration time, in milliseconds, for the given chunk. Once this
 * time has elapsed, the chunk might become unreadable to other clients.
 * \param chunk pointer to an allocated \c speculo_chunk.
 * \param ms number of milliseconds for which the chunk must remain readable.
 * \attention this function can be called only after speculo_chunk_allocate()
 * has been called on \a chunk.
 */
void speculo_chunk_set_expiration_time(speculo_chunk *chunk, long ms)
{
    speculo_chunk_header *chunk_header;
    speculo_area_header *area_header;
    struct timespec *chunk_expiration_time;
    div_t seconds;

    assert(chunk->area != NULL);
    assert(chunk->base_offset != 0);

    chunk_header = CHUNK_HEADER(chunk);
    assert(chunk->id == chunk_header->id);
    chunk_expiration_time = &chunk_header->expiration_time;

    get_clock(chunk_expiration_time);

    /* Add ms milliseconds */
    seconds = div(ms, 1000);
    chunk_expiration_time->tv_sec += seconds.quot;
    chunk_expiration_time->tv_nsec += seconds.rem;
    if (chunk_expiration_time->tv_nsec > 1000 * 1000 * 1000) {
        chunk_expiration_time->tv_sec++;
        chunk_expiration_time->tv_nsec -= 1000 * 1000 * 1000;
    }

    /* Update next expiration time */
    area_header = AREA_HEADER(chunk->area);
    if (unlikely(clock_compare(chunk_expiration_time,
                               &area_header->next_expiration_time) < 0)) {
        area_header->next_expiration_time = *chunk_expiration_time;
    }
}

/*!
 * Commits the chunk data to memory. After this function has been called, \a
 * chunk becomes visible to other speculo clients.
 * \param chunk pointer to an allocated \c speculo_chunk.
 */
void speculo_chunk_commit(speculo_chunk *chunk)
{
    speculo_chunk_header *chunk_header;

    assert(chunk->area != NULL);
    assert(chunk->base_offset != 0);

    chunk_header = CHUNK_HEADER(chunk);
    assert(chunk->id == chunk_header->id);
    ATOMIC_STORE(&chunk_header->state, CHUNK_WRITTEN);

    if (chunk_header->offset_to_obsoleted != 0) {
        /* Find old chunk and obsolete it. If "offset_to_obsoleted" contains a
         * real offset, the search is immediate; otherwise, we need to run
         * through the whole area and find the old chunk */

        speculo_chunk_header *obsoleted_chunk_header;

        if (chunk_header->offset_to_obsoleted == (size_t) -1) {
            speculo_area_header *area_header = AREA_HEADER(chunk->area);
            enum state_t state;

            obsoleted_chunk_header = NULL;
            while ((obsoleted_chunk_header =
                    area_get_next_chunk_header(area_header,
                                               obsoleted_chunk_header,
                                               &state)) != chunk_header) {
                if (obsoleted_chunk_header->update_id == chunk->update_id) {
                    obsoleted_chunk_header->state = CHUNK_OBSOLETED;
                }
            }
        } else {
            obsoleted_chunk_header = (speculo_chunk_header *)
                ((char *)chunk_header - chunk_header->offset_to_obsoleted);
            assert(obsoleted_chunk_header->update_id == chunk->update_id);
            obsoleted_chunk_header->state = CHUNK_OBSOLETED;
        }
    }
}

/*!
 * Updates a committed chunk. This effectively obsoletes the existing chunk and
 * creates a new chunk which replaces it. Readers who had a handle to the
 * previous chunk will be informed that it has been obsoleted.
 * \param chunk pointer to a committed \c speculo_chunk.
 * \param size the number of bytes to allocate.
 * \return a pointer to the newly allocated memory, or \c NULL if an error occurred.
 * \sa speculo_chunk_commit
 */
void *speculo_chunk_update(speculo_chunk *chunk, size_t size)
{
    speculo_chunk_header *chunk_header = NULL;
    speculo_chunk_header *new_chunk_header;
    speculo_area_header *area_header;
    speculo_area *area = chunk->area;
    enum state_t chunk_state;
    char saved_shm_suffix;
    void *ptr;

    assert(area != NULL);
    assert(chunk->base_offset != 0);

    /* We manually find the chunk in the SHM object because we cannot trust
     * the "base_address" field: the SHM area might have been recompacted, or
     * the chunk handle we are given has already been obsoleted.
     */
    area_header = AREA_HEADER(area);
    while ((chunk_header = area_get_next_chunk_header(area_header,
                                                      chunk_header,
                                                      &chunk_state)) != NULL) {
        if (chunk_header->id == chunk->id &&
            chunk_state == CHUNK_WRITTEN) {
            break;
        }
    }

    /* If the old chunk was not found, something is very wrong */
    assert(chunk_header != NULL);
    area_header->discarded_size +=
        sizeof(speculo_chunk_header) + align_size(chunk_header->size);

    /* Allocate space for the new modified chunk. */
    saved_shm_suffix = area->x.w.shm_suffix;
    chunk->base_offset = 0;
    ptr = speculo_chunk_allocate(chunk, size);
    if (unlikely(ptr == NULL)) return NULL;

    /* we must remember that the newly allocated chunk is an update, so that
     * we'll mark the old one as obsolete in speculo_chunk_commit().
     * If the area has not been recompacted, we can store the offset to the
     * obsoleted chunk in the chunk header. Otherwise, we'll have to search for
     * the old chunk linearly. */
    new_chunk_header = CHUNK_HEADER(chunk);
    if (area->x.w.shm_suffix == saved_shm_suffix) {
        new_chunk_header->offset_to_obsoleted =
            (char *)new_chunk_header - (char *)chunk_header;
    } else {
        new_chunk_header->offset_to_obsoleted = (size_t) -1;
    }

    return ptr;
}

/*!
 * Destroys the shared area. Clients won't be able to read more chunks from it;
 * however, clients who are in the middle of a read (that is, who have called
 * speculo_chunk_read() but not yet speculo_chunk_read_done()) will be able to
 * complete the operation.
 * \param area pointer to a \c speculo_area.
 */
int speculo_area_destroy(speculo_area *area)
{
    int ret;

    assert(area != NULL);

    if (area->base_address != NULL) {
        assert(area->x.w.max_size > 0);
        munmap(area->base_address, area->x.w.max_size);
    }

    ret = speculo_area_unlink(area->name);
    if (unlikely(ret < 0)) return -1;

    free(area);
    return 0;
}

/*!
 * Deletes a shared area by name.
 * \param name the name of the shared area to remove.
 */
int speculo_area_unlink(const char *name)
{
    char manifest_path[MAX_MANIFEST_PATH_LENGTH];
    char shmem_name[MAX_SHMEM_NAME_LENGTH];
    int manifest;
    size_t read_bytes;
    int length;

    manifest_path_from_area_name(name, manifest_path, sizeof(manifest_path));
    manifest = open(manifest_path, O_RDONLY);
    if (unlikely(manifest == -1)) return -1;

    length = shmem_name_from_area_name(name, shmem_name, sizeof(shmem_name));
    read_bytes = read(manifest, shmem_name + length - 1, 1);
    if (likely(read_bytes == 1)) {
        shm_unlink(shmem_name);
    }

    close(manifest);
    return unlink(manifest_path);
}

/* ========================= Reader APIs ========================== */

/* Returns the file descriptor of the SHM object */
static int area_map_read_manifest(speculo_area *area, const char *manifest_path)
{
    char shmem_name[MAX_SHMEM_NAME_LENGTH];
    int manifest;
    size_t read_bytes;
    int length;
    int fd;

    manifest = open(manifest_path, O_RDONLY);
    if (unlikely(manifest == -1)) return -1;

    length = shmem_name_from_area_name(area->name, shmem_name, sizeof(shmem_name));

    /* The manifest file consists of a single byte, which identifies the SHM
     * area to read: it is the last character of the filename which we must
     * pass to shm_open().
     * So, now we read a byte from the manifest file, and place it at the very
     * end of shmem_name (overwriting the last character). */
    read_bytes = read(manifest, shmem_name + length - 1, 1);
    if (unlikely(read_bytes < 1)) return -1;

    /* This should really exist */
    fd = shm_open(shmem_name, O_RDONLY, 0);
    close(manifest);

    return fd;
}

static int area_map_open_shm(speculo_area *area)
{
    char manifest_path[MAX_MANIFEST_PATH_LENGTH];
    struct stat file_stats;
    char *base_address;
    int retries;
    int fd;

    manifest_path_from_area_name(area->name, manifest_path, sizeof(manifest_path));

    retries = 0;
    do {
        fd = area_map_read_manifest(area, manifest_path);
        if (likely(fd >= 0)) break;
        if (retries++ > 5) return -1;
        sched_yield();
    } while (fd < 0);

    /* get the maximum size of the SHM area */
    if (unlikely(fstat(fd, &file_stats) < 0 ||
                 file_stats.st_size <= AREA_INITIAL_SIZE)) {
        close(fd);
        return -1;
    }

    base_address = mmap(NULL, file_stats.st_size,
                        PROT_READ,
                        MAP_SHARED, fd, 0);
    close(fd);
    if (unlikely(base_address == NULL)) return -1;

    if (area->base_address != NULL) {
        /* Now we need to unmap the previously mapped region. However, we can do
         * that only if no chunks are currently being read from it.
         */
        if (unlikely(area->x.r.held_chunks != 0)) {
            speculo_obsoleted_area *obsoleted_area;

            assert(area->x.r.held_chunks > 0);

            /* The region is still in use: save its data so that it can be unmapped
             * when all its read locks are released. */
            obsoleted_area = malloc(sizeof(speculo_obsoleted_area));
            if (unlikely(obsoleted_area == NULL)) {
                /* Not sure how to handle this. Just forget about unmapping. */
                goto done_unmapping;
            }

            obsoleted_area->base_address = area->base_address;
            obsoleted_area->mapped_size = area->x.r.mapped_size;
            obsoleted_area->held_chunks = area->x.r.held_chunks;
            LIST_INSERT_HEAD(&area->x.r.obsoleted_areas, obsoleted_area, list);
        } else {
            munmap(area->base_address, area->x.r.mapped_size);
        }
    }

done_unmapping:
    /* Update the local data about the area */
    area->base_address = base_address;
    area->x.r.held_chunks = 0;
    area->x.r.mapped_size = file_stats.st_size;
    return 0;
}

/*
 * Check that the area is not obsoleted; if it is, load the new SHM object.
 */
static int area_map_check_valid(speculo_area *area)
{
    speculo_area_header *area_header;
    int state;

    assert(area->base_address != NULL);
    area_header = AREA_HEADER(area);
    state = ATOMIC_LOAD(&area_header->state);
    if (unlikely(state != AREA_STATE_READY)) {
        if (unlikely(state >= AREA_STATE_LAST)) {
            /* This should never happen, unless the writer is buggy */
            return -1;
        }
        if (state == AREA_STATE_OBSOLETED) {
            if (area_map_open_shm(area) < 0) {
                return -1;
            }
            area_header = AREA_HEADER(area);
        } else {
            /* NOTE: this is very unlikely to happen, and if it happens it
             * should be just for a short time; this is why we allow the
             * iteration to run only a few times. OTOH, we don't want to
             * iterate forever because it might be that the writer has
             * crashed. */
            int i = 0;
            do {
                sched_yield();
                state = ATOMIC_LOAD(&area_header->state);
                i++;
            } while (state == AREA_STATE_INITIALIZING && i < 5);
            if (unlikely(state != AREA_STATE_READY)) return -1;
        }
    }
    return 0;
}

/*!
 * Opens a shared memory area for reading.
 * \param name the name of the area (no more than SPECULO_AREA_MAX_NAME_LENGTH
 * characters).
 * \return a pointer to a \c speculo_area structure. speculo_area_close() must
 * be called once done reading the area.
 */
speculo_area *speculo_area_open(const char *name)
{
    char manifest_path[MAX_MANIFEST_PATH_LENGTH];
    speculo_area *area = NULL;
    int ret;

    speculo_initialize();

    manifest_path_from_area_name(name, manifest_path, sizeof(manifest_path));
    if (unlikely(access(manifest_path, R_OK) != 0)) return NULL;

    area = malloc(sizeof(speculo_area));
    if (unlikely(area == NULL)) return NULL;

    strncpy(area->name, name, SPECULO_AREA_MAX_NAME_LENGTH + 1);
    area->name[SPECULO_AREA_MAX_NAME_LENGTH] = '\0';
    area->base_address = NULL;
    area->x.r.held_chunks = 0;
    LIST_INIT(&area->x.r.obsoleted_areas);

    ret = area_map_open_shm(area);
    if (unlikely(ret < 0)) {
        free(area);
        return NULL;
    }

    return area;
}

/*!
 * Initializes a \c speculo_chunk structure to iterate through the shared
 * memory area contents.
 * \param area pointer to a \c speculo_area.
 * \param chunk pointer to an uninitialized \c speculo_chunk structure.
 */
void speculo_area_init_chunk(speculo_area *area, speculo_chunk *chunk)
{
    assert(area != NULL);
    assert(chunk != NULL);

    chunk->area = area;
    chunk->id = 0;
    chunk->update_id = 0;
    chunk->area_id = 0;
    chunk->base_offset = 0;
}

/*!
 * Positions the \a chunk cursor to the next chunk available for reading.
 * \param chunk pointer to an initialized \c speculo_chunk structure.
 * \return \c 0 if \a chunk now points to a valid memory chunk, \c -1 on error.
 * \sa speculo_area_init_chunk, speculo_chunk_get_next
 */
int speculo_chunk_get_next(speculo_chunk *chunk)
{
    speculo_area_header *area_header;
    speculo_chunk_header *chunk_header;
    enum state_t chunk_state;
    int ret;

    assert(chunk != NULL);
    assert(chunk->area != NULL);

    ret = area_map_check_valid(chunk->area);
    if (unlikely(ret < 0)) return ret;

    area_header = AREA_HEADER(chunk->area);

    /*
     * Determine the starting point:
     * Uninitialized chunk: get the first valid chunk
     * Others:
     *   Check area id and chunk area_id: if matching, start scanning from
     *   chunk->base_offset, otherwise from start (because the offset is no
     *   longer valid)
     *   Find first chunk with id > chunk->id
     */
    if (chunk->id == 0) {
        /* We are reading the first chunk in the area */
        assert(chunk->area_id == 0);
        chunk_header = NULL;
    } else if (chunk->area_id != area_header->id) {
        assert(chunk->area_id < area_header->id);
        chunk_header = NULL;
    } else {
        assert(chunk->base_offset != 0);
        chunk_header = CHUNK_HEADER(chunk);
    }

    while ((chunk_header = area_get_next_chunk_header(area_header,
                                                      chunk_header,
                                                      &chunk_state)) != NULL) {
        if (chunk_state == CHUNK_WRITTEN &&
            chunk_header->id > chunk->id) {
            chunk->id = chunk_header->id;
            chunk->update_id = chunk_header->update_id;
            chunk->area_id = area_header->id;
            chunk->base_offset = CHUNK_OFFSET(chunk_header, area_header);
            return 0;
        }

        /* If we find an allocated (but not committed) chunk we need to
         * stop iterating: otherwise, if we are scheduled out and the
         * writer continues to write more chunks, on the next iteration of
         * this loop we'd be reading a valid chunk, while skipping the
         * current one which in the meantime has also become valid.
         */
        if (chunk_state == CHUNK_ALLOCATED) break;
    }

    return -1;
}

/*!
 * Reads the contents of a chunk. The client must call
 * speculo_chunk_read_done() once it's done reading the data.
 * \param chunk pointer to an initialized \c speculo_chunk structure.
 * \param data pointer to a location which will receive the address of the chunk
 * data.
 * \param size pointer to a location which will receive the size of the chunk
 * data.
 * \return \c 0 if successful, \c -1 on error. The value \c 1 is returned if
 * the operation is successful but the chunk has been obsoleted (this must have
 * happened between the call to speculo_chunk_get_next() and
 * speculo_chunk_read(). In that case, you can use speculo_chunk_read_latest()
 * to get the updated version of the chunk data.
 * \sa speculo_chunk_read_done, speculo_chunk_read_latest
 */
int speculo_chunk_read(speculo_chunk *chunk, void **data, size_t *size)
{
    speculo_chunk_header *chunk_header;
    enum state_t state;

    assert(chunk != NULL);
    assert(chunk->area != NULL);
    assert(chunk->base_offset != 0);
    assert(data != NULL);
    assert(size != NULL);

    if (unlikely(chunk->area_id != ATOMIC_LOAD(&(AREA_HEADER(chunk->area)->id)))) {
        /* The base_offset is no longer valid; we must find where this chunk
         * lies in the new memory area (it might also not be there anymore).
         *
         * We use the speculo_chunk_get_next function to read the next chunk,
         * starting from a chunk ID which is one less than the current one. And
         * when the function returns, we check whether we actually got our
         * chunk.
         */
        speculo_chunk new_chunk = *chunk;
        new_chunk.id--;
        if (unlikely(speculo_chunk_get_next(&new_chunk) != 0)) return -1;
        if (new_chunk.update_id != chunk->update_id) return -1;

        /* We got our chunk. Update its base_address and area_id */
        *chunk = new_chunk;
    }

    /* What to do if the chunk data has has been obsoleted?
     * Ideally, we would want to read the latest version of the data, but
     * that's not trivial to implement: it would require some bookkeeping in
     * order to avoir skipping chunks or reading the same chunk twice.
     * So, we simply choose to return the old data, and signal the situation by
     * using a different return value.
     */
    chunk_header = CHUNK_HEADER(chunk);
    *size = chunk_header->size;
    *data = (char *)chunk_header + sizeof(speculo_chunk_header);
    chunk->area->x.r.held_chunks++;
    state = ATOMIC_LOAD(&chunk_header->state);
    return (state == CHUNK_WRITTEN) ? 0 : 1;
}

/*!
 * Reads the contents of a chunk. If the chunk has been updated, get the latest
 * version of the data. The client must call speculo_chunk_read_done() once
 * it's done reading the data.
 * \note This function can modify the value of the \a chunk structure in such a
 * way that calling speculo_chunk_get_next() on it might lead to skipping some
 * chunks.
 * \param chunk pointer to an initialized \c speculo_chunk structure.
 * \param data pointer to a location which will receive the address of the chunk
 * data.
 * \param size pointer to a location which will receive the size of the chunk
 * data.
 * \return \c 0 if successful, \c -1 on error. The value \c 1 is returned if
 * the chunk has successfully been read but its contents are expired and won't
 * be updated anymore: in this case the client is advised to ignore the data.
 * \sa speculo_chunk_read_done
 */
int speculo_chunk_read_latest(speculo_chunk *chunk, void **data, size_t *size)
{
    speculo_area_header *area_header;
    speculo_chunk_header *chunk_header;
    speculo_chunk_header *latest_chunk = NULL;
    enum state_t state;
    int ret;

    assert(chunk != NULL);
    assert(chunk->area != NULL);

    ret = area_map_check_valid(chunk->area);
    if (unlikely(ret < 0)) return ret;

    area_header = AREA_HEADER(chunk->area);
    if (unlikely(area_header->id != chunk->area_id)) {
        /* If the area has changed, we must find our chunk again. */
        chunk_header = NULL;
    } else {
        chunk_header = CHUNK_HEADER(chunk);

        /* Check if our chunk is still valid */
        state = ATOMIC_LOAD(&chunk_header->state);
        if (state == CHUNK_WRITTEN || state == CHUNK_EXPIRED) {
            *size = chunk_header->size;
            *data = (char *)chunk_header + sizeof(speculo_chunk_header);
            chunk->area->x.r.held_chunks++;
            return state == CHUNK_WRITTEN ? 0 : 1;
        }
    }

    int seen_chunks = 0;
    /* The chunk has been obsoleted. Look for an updated version */
    while ((chunk_header = area_get_next_chunk_header(area_header,
                                                      chunk_header,
                                                      &state)) != NULL) {
        seen_chunks++;
        if (chunk_header->update_id == chunk->update_id) {
            if (state == CHUNK_WRITTEN) {
                latest_chunk = chunk_header;
                break;
            } else if (state == CHUNK_OBSOLETED) {
                latest_chunk = chunk_header;
            }
        }
    }

    /* If the chunk had expired and the area has been recompacted, the chunk
     * won't be found.
     */
    if (unlikely(!latest_chunk)) return -1;

    /* It is possible to arrive here without having found a chunk with
     * CHUNK_WRITTEN state, if the area has been obsoleted in the meantime that
     * this function executed. We could start again and get the new SHM object,
     * however if the writer is continuously updating the area, this could
     * cause us to never return.
     * Therefore, just return the latest version of che chunk data which we
     * found in this SHM object.
     */
    chunk->id = latest_chunk->id;
    chunk->base_offset = CHUNK_OFFSET(latest_chunk, area_header);
    *size = latest_chunk->size;
    *data = (char *)latest_chunk + sizeof(speculo_chunk_header);
    chunk->area->x.r.held_chunks++;
    return 0;
}

/*!
 * Completes the reading of a chunk. Every successful call to
 * speculo_chunk_read must eventually be paired with a call to this function.
 * \param chunk pointer to the chunk which has been read.
 */
void speculo_chunk_read_done(speculo_chunk *chunk)
{
    speculo_area *area;

    assert(chunk != NULL);
    area = chunk->area;
    assert(area != NULL);
    if (unlikely(chunk->area_id != AREA_HEADER(area)->id)) {
        speculo_obsoleted_area *obsoleted_area;
        for (obsoleted_area = area->x.r.obsoleted_areas.lh_first;
             obsoleted_area != NULL;
             obsoleted_area = obsoleted_area->list.le_next)
        {
            speculo_area_header *area_header =
                (speculo_area_header *)obsoleted_area->base_address;
            if (area_header->id == chunk->area_id) {
                obsoleted_area->held_chunks--;
                if (obsoleted_area->held_chunks <= 0) {
                    assert(obsoleted_area->held_chunks == 0);
                    /* The area can be unmapped */
                    munmap(obsoleted_area->base_address,
                           obsoleted_area->mapped_size);
                    LIST_REMOVE(obsoleted_area, list);
                }
                break;
            }
        }
    } else {
        chunk->area->x.r.held_chunks--;
    }
}

/*!
 * Closes \a area and release all associated resources.
 * \param area a pointer to a \c speculo_area
 */
void speculo_area_close(speculo_area *area)
{
    assert(area != NULL);

    if (area->base_address != NULL) {
        assert(area->x.r.mapped_size > 0);
        munmap(area->base_address, area->x.r.mapped_size);
    }

    while (area->x.r.obsoleted_areas.lh_first != NULL)
    {
        speculo_obsoleted_area *obsoleted_area =
            area->x.r.obsoleted_areas.lh_first;

        munmap(obsoleted_area->base_address, obsoleted_area->mapped_size);
        LIST_REMOVE(obsoleted_area, list);
    }

    free(area);
}
