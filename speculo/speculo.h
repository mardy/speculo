/* vi: set et sw=4 ts=4 cino=t0,(0: */
/*
 * Copyright (C) 2013 Alberto Mardegan <info@mardy.it>
 *
 * This file is part of libspeculo.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 */

#ifndef SPECULO_SPECULO_H
#define SPECULO_SPECULO_H

#include <stddef.h>

typedef struct speculo_area_t speculo_area;
typedef struct speculo_chunk_t speculo_chunk;

struct speculo_chunk_t {
    /* private */
    speculo_area *area;
    int id;
    int update_id;
    int area_id;
    size_t base_offset;
};

/* If the SHM area already exists, normally speculo_area_create() will truncate
 * it and start a new one. With this flag, speculo_area_create() will just
 * fail. */
#define SPECULO_MODE_EXCLUSIVE 0x1

speculo_area *speculo_area_new(const char *name, int flags);
int speculo_area_create(speculo_area *area);
int speculo_area_set_max_size(speculo_area *area, size_t size);
void speculo_area_set_max_trash_percentage(speculo_area *area,
                                           int percentage);
void speculo_area_set_min_compact_size(speculo_area *area, size_t size);

int speculo_area_create_chunk(speculo_area *area, speculo_chunk *chunk);

void *speculo_chunk_allocate(speculo_chunk *chunk, size_t size);
#define speculo_chunk_allocate_struct(chunk, type) \
    (type *)speculo_chunk_allocate(chunk, sizeof(type))

void speculo_chunk_set_expiration_time(speculo_chunk *chunk, long ms);

void speculo_chunk_commit(speculo_chunk *chunk);

void *speculo_chunk_update(speculo_chunk *chunk, size_t size);
#define speculo_chunk_update_struct(chunk, type) \
    (type *)speculo_chunk_update(chunk, sizeof(type))

int speculo_area_destroy(speculo_area *area);
int speculo_area_unlink(const char *name);

speculo_area *speculo_area_open(const char *name);

void speculo_area_init_chunk(speculo_area *area, speculo_chunk *chunk);

/* @chunk: in input, existing chunk or new one initialized with
 * speculo_chunk_init(). */
int speculo_chunk_get_next(speculo_chunk *chunk);

int speculo_chunk_read(speculo_chunk *chunk, void **data, size_t *size);
int speculo_chunk_read_latest(speculo_chunk *chunk, void **data, size_t *size);
void speculo_chunk_read_done(speculo_chunk *chunk);

void speculo_area_close(speculo_area *area);

#endif /* SPECULO_SPECULO_H */
