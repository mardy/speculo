# -*- Makefile -*-
# This file contains the detection rules
all:

PKGCONFIG=pkg-config
TAR=tar

CONFIGFILE = config.cache
ifeq ($(CONFIGURING),1)

TOP_BUILD_DIR = $(shell pwd)

# Detect the target system
# Ask the compiler what OS it's producing files for
UNAME := $(shell $(CC) -dumpmachine 2>&1 | grep -E -o "linux|darwin|win|gnu|kfreebsd")

# Compiler flags
EXTRA_CFLAGS += -fPIC

# Find needed and optional libraries
CHECK = $(shell $(PKGCONFIG) --libs check 2> /dev/null)
CHECK_CFLAGS = $(shell $(PKGCONFIG) --cflags check 2> /dev/null)

LCOV = $(shell lcov -v 2>&1 > /dev/null && echo "lcov")
GENHTML = $(shell genhtml -v 2>&1 > /dev/null && echo "genhtml")

DOXYGEN = $(shell doxygen --version 2>&1 > /dev/null && echo "doxygen")

ifeq ($(COVERAGE),1)
	ifeq ($(strip $(LCOV)),)
		$(error lcov not found)
	endif
	ifeq ($(strip $(CHECK)),)
		$(error check not found)
	endif
	COVERAGE_CFLAGS += -ftest-coverage --coverage -DNDEBUG
   	EXTRA_CFLAGS += -O0 -g
	COVERAGE_LDFLAGS += -lgcov
endif

# Write the configure file
all: configure
configure $(CONFIGURE): Configure.mk
	@echo "\
	CONFIGURED = 1\\\
	UNAME = $(UNAME)\\\
	LCOV = $(LCOV)\\\
	GENHTML = $(GENHTML)\\\
	CHECK = $(CHECK)\\\
	CHECK_CFLAGS = $(CHECK_CFLAGS)\\\
	CFLAGS =$(CFLAGS)\\\
	COVERAGE_CFLAGS = $(COVERAGE_CFLAGS)\\\
	COVERAGE_LDFLAGS = $(COVERAGE_LDFLAGS)\\\
	DOXYGEN = $(DOXYGEN)\\\
	EXTRA_CFLAGS = $(EXTRA_CFLAGS)\\\
	EXTRA_LDFLAGS = $(EXTRA_LDFLAGS)\\\
	TOP_BUILD_DIR = $(TOP_BUILD_DIR)\\\
	" | tr '\\' '\n' > $(CONFIGFILE)

else
configure $(CONFIGFILE): Configure.mk
	@test -e $(CONFIGFILE) && echo Reconfiguring... || echo Configuring...
	@$(MAKE) CONFIGURING=1 configure
	@echo Done

-include $(CONFIGFILE)
endif

.PHONY: configure all
