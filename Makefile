NAME = speculo

include Configure.mk
VERSION = 0.1

CC = gcc
EXTRA_CFLAGS += -Wall
INSTALL = install

# prefix = $(HOME)
prefix = $(DESTDIR)/usr
LIBDIR = $(prefix)/lib # FIXME: make overridable
DATADIR = $(prefix)/share
DOCDIR = $(DATADIR)/doc/$(NAME)
MANDIR = $(DATADIR)/man/man3

MANFILES = docs/man/man3

HEADERS = \
	speculo/config.h \
	speculo/speculo.h

SOURCES = \
	speculo/speculo.c

TARGET = speculo/lib$(NAME).so

LIBS = -lrt

# Unit tests
TEST_SOURCES = \
	tests/unit-tests.c
TEST_CFLAGS = $(CHECK_CFLAGS)
TEST_LIBS = $(CHECK) -lrt
TEST_TARGET = tests/unit-tests

include Rules.mk
