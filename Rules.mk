# -*- Makefile -*-

ifeq ($(V),1)
	PRETTYECHO=true
	COMPILE_PREFIX=
else
	PRETTYECHO=echo
	COMPILE_PREFIX=@
endif

C_SOURCES = $(filter %.c, $(SOURCES) $(TEST_SOURCES))
OBJS = $(C_SOURCES:.c=.o)
SPECULO_OBJS = $(SOURCES:.c=.o)
TEST_OBJS = $(TEST_SOURCES:.c=.o)

all: $(TARGET)

$(TARGET): $(SPECULO_OBJS)
	@$(PRETTYECHO) '    LINK' $@
	$(COMPILE_PREFIX)$(CC) $(LDFLAGS) -shared -o $@ $^ $(LIBS) $(EXTRA_LDFLAGS)

$(TEST_TARGET): EXTRA_CFLAGS += $(COVERAGE_CFLAGS) $(TEST_CFLAGS)
$(TEST_TARGET): $(TEST_OBJS)
	@$(PRETTYECHO) '    LINK' $@
	$(COMPILE_PREFIX)$(CC) $(LDFLAGS) -o $@ $^ $(TEST_LIBS) $(EXTRA_LDFLAGS) $(COVERAGE_LDFLAGS)

.PHONY: check
check: $(TEST_TARGET)
	$(TEST_TARGET)

# Coverage rules
#
.PHONY: clean-gcda coverage-html generate-coverage-html clean-coverage-html
clean-gcda:
	@echo Removing old coverage results
	-find -name '*.gcda' -print | xargs -r rm

coverage-html: clean-gcda
	-TEST_CASES="core" $(MAKE) -k check
	$(MAKE) generate-coverage-html

generate-coverage-html:
	@echo Collecting coverage data
	$(LCOV) --base-directory $(TOP_BUILD_DIR) --directory $(TOP_BUILD_DIR) --capture --output-file coverage.info.tmp --no-checksum --compat-libtool
	$(LCOV) -e coverage.info.tmp "$(TOP_BUILD_DIR)/speculo/*" -q > coverage.info
	LANG=C $(GENHTML) --prefix $(TOP_BUILD_DIR) --output-directory coveragereport --title "Code Coverage" --legend --show-details coverage.info

clean-coverage-html: clean-gcda
	-$(LCOV) --directory $(TOP_BUILD_DIR) -z
	-find -name '*.gcno' -print | xargs -r rm
	-rm -rf coverage.info* coveragereport


install: all
	$(INSTALL) -d -m 755 $(LIBDIR)
	$(INSTALL) $(TARGET)* $(LIBDIR)
	$(INSTALL) -d -m 755 $(MANDIR)
	$(INSTALL) -m 644 $(wildcard $(MANFILES)/*) $(MANDIR)
	$(INSTALL) -d -m 755 $(DOCDIR)
	cp -r docs/html/ $(DOCDIR)

%.o: %.c
	@$(PRETTYECHO) '    CC' $<
	@mkdir -p .dep/$(@D)
	$(COMPILE_PREFIX)$(CC) $(CFLAGS) $(EXTRA_CFLAGS) -MD -MF .dep/$@.dep -c -o $@ $<

doc: $(SOURCES)
ifeq ($(strip $(DOXYGEN)),)
	@echo Doxygen not found, building of documentation disabled
else
	$(DOXYGEN) doxy.conf
endif

clean: clean-coverage-html
	rm -f $(OBJS) *~ $(TARGET) $(TEST_TARGET)

confclean: clean
	rm -f $(CONFIGFILE)
	rm -rf .dep

distclean: confclean

release:
	git archive --prefix $(NAME)-$(VERSION)/ \
		    --output $(NAME)-$(VERSION).tgz \
		    v$(VERSION)

ifneq ($(CONFIGURED)$(CONFIGURING),)
.dep/%.o.dep: %.cpp
	@mkdir -p $(@D)
	@$(CXX) $(CXXFLAGS) $(EXTRA_FLAGS) -MM -MG -MF $@ -MT $(<:.cpp=.o) -c $<
endif

DEPS = $(addprefix .dep/,$(C_SOURCES:.c=.o.dep))
-include $(DEPS)
