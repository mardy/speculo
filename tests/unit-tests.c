#include <check.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "../speculo/speculo.c"

#if CHECK_MAJOR_VERSION > 0 || \
    (CHECK_MAJOR_VERSION == 0 && \
     (CHECK_MINOR_VERSION > 9 || \
      (CHECK_MINOR_VERSION == 9 && CHECK_MICRO_VERSION >= 12)))
#define _fail_unless(expr, file, line, ...) \
    (expr) ? \
        _mark_point(file, line) : \
        _ck_assert_failed(file, line, "Assertion '"#expr"' failed", ## __VA_ARGS__, NULL)
#elif CHECK_MAJOR_VERSION > 0 || \
    (CHECK_MAJOR_VERSION == 0 && \
     (CHECK_MINOR_VERSION > 9 || \
      (CHECK_MINOR_VERSION == 9 && CHECK_MICRO_VERSION >= 10)))
#define _fail_unless _ck_assert_msg
#endif

static char get_shm_id(speculo_area *area)
{
    FILE *manifest;
    char manifest_path[MAX_MANIFEST_PATH_LENGTH];
    char id;
    size_t read_bytes;

    manifest_path_from_area_name(area->name, manifest_path,
                                 sizeof(manifest_path));
    manifest = fopen(manifest_path, "r");
    ck_assert(manifest != NULL);
    read_bytes = fread(&id, 1, 1, manifest);
    ck_assert_int_eq(read_bytes, 1);
    fclose(manifest);

    return id;
}

static unsigned int
timespec_diff_ms(struct timespec *ts1, struct timespec *ts0)
{
    return (ts1->tv_sec - ts0->tv_sec) * 1000 +
        (ts1->tv_nsec - ts0->tv_nsec) / 1000000;
}

START_TEST(test_reader_open_unexisting)
{
    speculo_area *area;

    area = speculo_area_open("");
    ck_assert(area == NULL);

    area = speculo_area_open("nonexistingarea");
    ck_assert(area == NULL);
}
END_TEST

START_TEST(test_open)
{
    speculo_area *area_w, *area_r;
    int ret;

    area_w = speculo_area_new("speculo-test", 0);
    ck_assert(area_w != NULL);

    /* The area is not created yet */
    area_r = speculo_area_open("speculo-test");
    ck_assert(area_r == NULL);

    ret = speculo_area_create(area_w);
    ck_assert_int_eq(ret, 0);

    area_r = speculo_area_open("speculo-test");
    ck_assert(area_r != NULL);

    speculo_area_close(area_r);

    speculo_area_destroy(area_w);
}
END_TEST

START_TEST(test_write_and_read)
{
    speculo_area *area_w, *area_r;
    static const char *chunks_data[] = {
        "first chunk",
        "second part, a bit longer",
        "something interesting here as well",
        NULL
    };
    speculo_chunk read_chunk;
    int i, ret;

    area_w = speculo_area_new("speculo-test", 0);
    ck_assert(area_w != NULL);

    ret = speculo_area_create(area_w);
    ck_assert_int_eq(ret, 0);

    for (i = 0; chunks_data[i] != NULL; i++) {
        speculo_chunk chunk;
        char *data_ptr;
        size_t length;

        ret = speculo_area_create_chunk(area_w, &chunk);
        ck_assert(ret == 0);

        length = strlen(chunks_data[i]) + 1;
        data_ptr = speculo_chunk_allocate(&chunk, length);
        ck_assert(data_ptr != NULL);

        memcpy(data_ptr, chunks_data[i], length);
        speculo_chunk_commit(&chunk);
    }

    /* Now read the data back */
    area_r = speculo_area_open("speculo-test");
    ck_assert(area_r != NULL);

    speculo_area_init_chunk(area_r, &read_chunk);
    for (i = 0; chunks_data[i] != NULL; i++) {
        void *read_data;
        size_t length;

        ret = speculo_chunk_get_next(&read_chunk);
        ck_assert(ret == 0);

        ret = speculo_chunk_read(&read_chunk, &read_data, &length);
        ck_assert(ret == 0);
        ck_assert_int_eq(length, strlen(chunks_data[i]) + 1);
        ck_assert(memcmp(read_data, chunks_data[i], length) == 0);
        speculo_chunk_read_done(&read_chunk);
    }

    /* attempt to read one more chunk; this should fail */
    ret = speculo_chunk_get_next(&read_chunk);
    ck_assert(ret < 0);

    speculo_area_close(area_r);

    speculo_area_destroy(area_w);
}
END_TEST

START_TEST(test_write_and_read_compact)
{
    speculo_area *area_w, *area_r;
    static const char *chunks_data[] = {
        "first chunk",
        "this will be overwritten",
        "second part, a bit longer",
        "this will expire soon",
        "something interesting here as well",
        NULL
    };
    static const char *chunks_data_read[] = {
        "first chunk",
        "second part, a bit longer",
        "something interesting here as well",
        NULL
    };
    speculo_chunk read_chunk, overwrite_chunk;
    int i, j, ret;

    area_w = speculo_area_new("speculo-test", 0);
    ck_assert(area_w != NULL);

    speculo_area_set_max_trash_percentage(area_w, 10);
    speculo_area_set_min_compact_size(area_w, 100);

    ret = speculo_area_create(area_w);
    ck_assert_int_eq(ret, 0);

    for (j = 0; j < 10; j++) {
        for (i = 0; chunks_data[i] != NULL; i++) {
            speculo_chunk chunk;
            char *data_ptr;
            size_t length;

            /* Sleep for one millisecond, to cause the expiration of the chunks
             * having i == 3 */
            usleep(1000);
            if (i != 4) {
                ret = speculo_area_create_chunk(area_w, &chunk);
                ck_assert(ret == 0);

                length = strlen(chunks_data[i]) + 1;
                data_ptr = speculo_chunk_allocate(&chunk, length);
                ck_assert(data_ptr != NULL);
            } else {
                chunk = overwrite_chunk;
                length = strlen(chunks_data[i]) + 1;
                data_ptr = speculo_chunk_update(&chunk, length);
                ck_assert(data_ptr != NULL);
            }

            if (i == 3)
                speculo_chunk_set_expiration_time(&chunk, 1);

            memcpy(data_ptr, chunks_data[i], length);
            speculo_chunk_commit(&chunk);

            if (i == 1)
                overwrite_chunk = chunk;
        }
    }

    /* Now read the data back; we shouldn't see any of the deleted and expired
     * chunks */
    area_r = speculo_area_open("speculo-test");
    ck_assert(area_r != NULL);

    speculo_area_init_chunk(area_r, &read_chunk);
    for (j = 0; j < 10; j++) {
        for (i = 0; chunks_data_read[i] != NULL; i++) {
            void *read_data;
            size_t length;

            ret = speculo_chunk_get_next(&read_chunk);
            ck_assert(ret == 0);

            ret = speculo_chunk_read(&read_chunk, &read_data, &length);
            ck_assert(ret == 0);
            ck_assert_int_eq(length, strlen(chunks_data_read[i]) + 1);
            ck_assert(memcmp(read_data, chunks_data_read[i], length) == 0);
            speculo_chunk_read_done(&read_chunk);
        }
    }

    /* attempt to read one more chunk; this should fail */
    ret = speculo_chunk_get_next(&read_chunk);
    ck_assert(ret < 0);

    speculo_area_close(area_r);

    speculo_area_destroy(area_w);
}
END_TEST

static speculo_chunk write_filled_chunk(speculo_area *area, size_t length,
                                        unsigned char content_byte)
{
    speculo_chunk chunk;
    unsigned char *data;
    int ret, i;

    ret = speculo_area_create_chunk(area, &chunk);
    ck_assert(ret == 0);
    data = speculo_chunk_allocate(&chunk, length);
    ck_assert(data != NULL);
    for (i = 0; i < length; i++)
        data[i] = content_byte;
    speculo_chunk_commit(&chunk);
    return chunk;
}

static void update_filled_chunk(speculo_chunk *chunk,
                                size_t length,
                                unsigned char content_byte)
{
    unsigned char *data;
    int i;

    data = speculo_chunk_update(chunk, length);
    ck_assert(data != NULL);
    for (i = 0; i < length; i++)
        data[i] = content_byte;
    speculo_chunk_commit(chunk);
}

#define assert_chunk_contents_match(chunk, expected_length, expected_content) \
    _assert_chunk_contents_match(__FILE__, __LINE__, \
                                 chunk, expected_length, expected_content, 0)
#define assert_chunk_latest_contents_match(chunk, expected_length, expected_content) \
    _assert_chunk_contents_match(__FILE__, __LINE__, \
                                 chunk, expected_length, expected_content, 1)
static void _assert_chunk_contents_match(const char *file, int line,
                                         speculo_chunk *chunk,
                                         size_t expected_length,
                                         unsigned char expected_content,
                                         int read_latest)
{
    unsigned char *data;
    size_t length;
    int ret, i;

    if (read_latest) {
        ret = speculo_chunk_read_latest(chunk, (void **)&data, &length);
    } else {
        ret = speculo_chunk_read(chunk, (void **)&data, &length);
    }
    _fail_unless(ret == 0, file, line, "Failed", "speculo_chunk_read returned %d", ret);
    _fail_unless(length == expected_length, file, line,
                 "Failed", "chunk length is %zd, expected %zd",
                 length, expected_length);
    for (i = 0; i < length; i++)
        _fail_unless(data[i] == expected_content, file, line,
                     "Failed", "Data differ: got 0x%hhx, expected 0x%hhx",
                     data[i], expected_content);
    speculo_chunk_read_done(chunk);
}

START_TEST(test_compact_between_reads)
{
    speculo_area *area_w, *area_r;
    speculo_chunk chunk_w, chunk_r;
    int ret;

    area_w = speculo_area_new("speculo-test", 0);
    ck_assert(area_w != NULL);

    speculo_area_set_max_trash_percentage(area_w, 80);
    speculo_area_set_min_compact_size(area_w, 1024 *2);
    speculo_area_set_max_size(area_w, 1024 * 16);

    ret = speculo_area_create(area_w);
    ck_assert_int_eq(ret, 0);

    /* first, write 1k of data */
    write_filled_chunk(area_w, 1024 * 1, 0x82);
    mark_point();

    /* next, write 4k of data */
    chunk_w = write_filled_chunk(area_w, 1024 * 4, 0xba);
    mark_point();

    /* now update that chunk with a 8k one */
    update_filled_chunk(&chunk_w, 1024 * 8, 0x58);

    /* now open the area for reading, and check that the updated chunk is not
     * there */
    area_r = speculo_area_open("speculo-test");
    ck_assert(area_r != NULL);

    speculo_area_init_chunk(area_r, &chunk_r);
    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret == 0);
    assert_chunk_contents_match(&chunk_r, 1024 * 1, 0x82);

    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret == 0);
    assert_chunk_contents_match(&chunk_r, 1024 * 8, 0x58);

    /* attempt to read one more chunk; this should fail */
    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret < 0);

    /* write a 6k chunk, this should trigger a recompactation */
    write_filled_chunk(area_w, 1024 * 6, 0xc6);
    mark_point();

    /* verify that we can read this chunk */
    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret == 0);

    assert_chunk_contents_match(&chunk_r, 1024 * 6, 0xc6);

    /* attempt to read one more chunk; this should fail */
    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret < 0);

    speculo_area_close(area_r);

    speculo_area_destroy(area_w);
}
END_TEST

START_TEST(test_compact_between_get_next_and_read)
{
    speculo_area *area_w, *area_r;
    speculo_chunk chunk_w, chunk_r, chunk_r_saved;
    int ret;

    area_w = speculo_area_new("speculo-test", 0);
    ck_assert(area_w != NULL);

    speculo_area_set_max_trash_percentage(area_w, 80);
    speculo_area_set_min_compact_size(area_w, 1024 *2);
    speculo_area_set_max_size(area_w, 1024 * 32);

    ret = speculo_area_create(area_w);
    ck_assert_int_eq(ret, 0);

    /* first, write 2k of data */
    write_filled_chunk(area_w, 1024 * 2, 0x82);

    /* first, write 8k of data */
    chunk_w = write_filled_chunk(area_w, 1024 * 8, 0xba);

    /* now update that chunk with a 16k one */
    update_filled_chunk(&chunk_w, 1024 * 16, 0x58);

    /* now open the area for reading, and check that the updated chunk is not
     * there */
    area_r = speculo_area_open("speculo-test");
    ck_assert(area_r != NULL);

    speculo_area_init_chunk(area_r, &chunk_r);
    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret == 0);
    assert_chunk_contents_match(&chunk_r, 1024 * 2, 0x82);

    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret == 0);
    assert_chunk_contents_match(&chunk_r, 1024 * 16, 0x58);

    /* attempt to read one more chunk; this should fail */
    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret < 0);

    /* write a 12k chunk, this should trigger a recompactation */
    write_filled_chunk(area_w, 1024 * 12, 0xc6);

    /* verify that we can read the previous chunk again */
    assert_chunk_contents_match(&chunk_r, 1024 * 16, 0x58);

    /* save this chunk's index, */
    chunk_r_saved = chunk_r;

    /* read the latest added chunk, */
    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret == 0);

    assert_chunk_contents_match(&chunk_r, 1024 * 12, 0xc6);

    /* and read the previous one again */
    assert_chunk_contents_match(&chunk_r_saved, 1024 * 16, 0x58);

    /* attempt to read one more chunk; this should fail */
    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret < 0);

    speculo_area_close(area_r);

    speculo_area_destroy(area_w);
}
END_TEST

static speculo_chunk write_filled_expiring_chunk(speculo_area *area, size_t length,
                                                 unsigned char content_byte,
                                                 int expire_in)
{
    speculo_chunk chunk;
    unsigned char *data;
    int ret, i;

    ret = speculo_area_create_chunk(area, &chunk);
    ck_assert(ret == 0);
    data = speculo_chunk_allocate(&chunk, length);
    ck_assert(data != NULL);
    for (i = 0; i < length; i++)
        data[i] = content_byte;
    speculo_chunk_set_expiration_time(&chunk, expire_in);
    speculo_chunk_commit(&chunk);
    return chunk;
}

START_TEST(test_compact_before_read_done)
{
    speculo_area *area_w, *area_r;
    speculo_chunk chunk_r, chunk_r_saved, chunk_r_saved2;
    speculo_chunk chunk_w1, chunk_w2, chunk_w3;
    unsigned char *data_r, *data_r_saved, *data_r_saved2;
    int i, ret;
    size_t length, length_saved, length_saved2;
    char area_id;

    area_w = speculo_area_new("speculo-test", 0);
    ck_assert(area_w != NULL);

    speculo_area_set_max_trash_percentage(area_w, 80);
    speculo_area_set_min_compact_size(area_w, 1024 *2);
    speculo_area_set_max_size(area_w, 1024 * 32);

    ret = speculo_area_create(area_w);
    ck_assert_int_eq(ret, 0);

    /* first, write 2k of data */
    chunk_w1 = write_filled_expiring_chunk(area_w, 1024 * 2, 0x82, 10000);

    /* next, write 8k of data, bound to expire soon */
    write_filled_expiring_chunk(area_w, 1024 * 8, 0xba, 2);

    /* 1k of data not expiring anytime soon */
    write_filled_expiring_chunk(area_w, 1024 * 1, 0x58, 20000);

    /* 2k of data expiring soon */
    write_filled_expiring_chunk(area_w, 1024 * 2, 0x40, 3);
    /* and again 2k */
    write_filled_expiring_chunk(area_w, 1024 * 2, 0x34, 2);

    /* 10k, not expiring */
    chunk_w2 = write_filled_expiring_chunk(area_w, 1024 * 10, 0x99, 14000);

    /* give some time for the chunks to expire */
    usleep(4000);

    /* 1k, expiring */
    write_filled_expiring_chunk(area_w, 1024 * 1, 0x11, 1);

    /* now open the area for reading, and check that the expired chunks are not
     * there */
    area_r = speculo_area_open("speculo-test");
    ck_assert(area_r != NULL);

    speculo_area_init_chunk(area_r, &chunk_r);
    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret == 0);
    assert_chunk_contents_match(&chunk_r, 1024 * 2, 0x82);

    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret == 0);
    assert_chunk_contents_match(&chunk_r, 1024 * 1, 0x58);

    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret == 0);
    assert_chunk_contents_match(&chunk_r, 1024 * 10, 0x99);
    /* we'll read this again soon */
    chunk_r_saved = chunk_r;

    /* Even if the last chunk has been set to expire soon, expiration won't
     * happen until a new chunk is allocated. So, we still can read it here. */
    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret == 0);
    assert_chunk_contents_match(&chunk_r, 1024 * 1, 0x11);

    /* attempt to read one more chunk; this should fail */
    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret < 0);

    /* now read a previous chunk again, but don't call read_done */
    ret = speculo_chunk_read(&chunk_r_saved, (void **)&data_r_saved, &length_saved);
    ck_assert(ret < 1);
    ck_assert_int_eq(length_saved, 1024 * 10);
    for (i = 0; i < length_saved; i++)
        ck_assert_int_eq(data_r_saved[i], 0x99);

    /* write a 12k chunk, this should trigger a recompactation */
    area_id = get_shm_id(area_w);
    chunk_w3 = write_filled_chunk(area_w, 1024 * 12, 0xc6);
    ck_assert_int_ne(get_shm_id(area_w), area_id);

    /* verify that the previous chunk has been obsoleted (ret == 1) */
    ret = speculo_chunk_read(&chunk_r, (void **)&data_r, &length);
    ck_assert(ret == 1);
    ck_assert_int_eq(length, 1024 * 1);
    for (i = 0; i < length; i++)
        ck_assert_int_eq(data_r[i], 0x11);
    speculo_chunk_read_done(&chunk_r);

    /* Attempt to read the last-written chunk.
     * This has also the effect of causing the reader area to be updated to
     * point at the new SHM object */
    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret == 0);
    assert_chunk_contents_match(&chunk_r, 1024 * 12, 0xc6);
    /* Save the index of this chunk, and read it again without calling
     * read_done.
     * Now we have two chunks, chunk_r_saved and chunk_r_saved2, for which
     * read_done was not called. */
    chunk_r_saved2 = chunk_r;
    ret = speculo_chunk_read(&chunk_r_saved2,
                             (void **)&data_r_saved2, &length_saved2);
    ck_assert(ret < 1);
    ck_assert_int_eq(length_saved2, 1024 * 12);
    for (i = 0; i < length_saved2; i++)
        ck_assert_int_eq(data_r_saved2[i], 0xc6);

    /* verify that the data of the first chunk for which we didn't call
     * read_done can still be read */
    for (i = 0; i < length_saved; i++)
        ck_assert_int_eq(data_r_saved[i], 0x99);
    mark_point();

    /* Now update some of the previously written chunks, and trigger another
     * recompactation */
    area_id = get_shm_id(area_w);
    update_filled_chunk(&chunk_w3, 1024 * 3, 0x49);
    update_filled_chunk(&chunk_w1, 1024 * 9, 0x21);
    update_filled_chunk(&chunk_w2, 1024 * 8, 0x37);
    update_filled_chunk(&chunk_w1, 1024 * 1, 0x23);
    ck_assert_int_ne(get_shm_id(area_w), area_id);

    /* verify that we can still read the data from the two opened chunks */
    mark_point();
    for (i = 0; i < length_saved; i++)
        ck_assert_int_eq(data_r_saved[i], 0x99);
    mark_point();
    for (i = 0; i < length_saved2; i++)
        ck_assert_int_eq(data_r_saved2[i], 0xc6);
    mark_point();

    /* Attempt to read the last-written chunks.
     * This has also the effect of causing the reader area to be updated to
     * point at the new SHM object */
    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret == 0);
    assert_chunk_contents_match(&chunk_r, 1024 * 3, 0x49);
    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret == 0);
    assert_chunk_contents_match(&chunk_r, 1024 * 8, 0x37);
    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret == 0);
    assert_chunk_contents_match(&chunk_r, 1024 * 1, 0x23);

    /* now inform speculo that we're done reading */
    speculo_chunk_read_done(&chunk_r_saved);
    speculo_chunk_read_done(&chunk_r_saved2);

    speculo_area_close(area_r);

    speculo_area_destroy(area_w);
}
END_TEST

START_TEST(test_read_latest)
{
    speculo_area *area_w, *area_r;
    speculo_chunk chunk_r, chunk_r2;
    speculo_chunk chunk_w1, chunk_w2, chunk_w3;
    int ret;

    area_w = speculo_area_new("speculo-test", 0);
    ck_assert(area_w != NULL);

    ret = speculo_area_create(area_w);
    ck_assert_int_eq(ret, 0);

    speculo_area_set_max_trash_percentage(area_w, 80);
    speculo_area_set_min_compact_size(area_w, 1024);
    speculo_area_set_max_size(area_w, 1024 * 16);

    chunk_w1 = write_filled_chunk(area_w, 1024 * 1, 0xae);
    chunk_w2 = write_filled_chunk(area_w, 1024 * 2, 0x82);
    chunk_w3 = write_filled_chunk(area_w, 1024 * 8, 0xba);

    /* now open the area for reading, and look for the chunk_w2 */
    area_r = speculo_area_open("speculo-test");
    ck_assert(area_r != NULL);

    speculo_area_init_chunk(area_r, &chunk_r);
    /* read the first chunk */
    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret == 0);
    assert_chunk_contents_match(&chunk_r, 1024 * 1, 0xae);

    /* the next should be chunk_w2 */
    ret = speculo_chunk_get_next(&chunk_r);
    ck_assert(ret == 0);
    assert_chunk_contents_match(&chunk_r, 1024 * 2, 0x82);
    chunk_r2 = chunk_r;

    /* write a few more uninteresting chunks */
    update_filled_chunk(&chunk_w3, 1024 * 1, 0x49);
    update_filled_chunk(&chunk_w1, 1024 * 2, 0x20);
    update_filled_chunk(&chunk_w3, 1024 * 3, 0x10);
    update_filled_chunk(&chunk_w3, 1024 * 1, 0x49);
    update_filled_chunk(&chunk_w1, 1024 * 2, 0x20);
    update_filled_chunk(&chunk_w3, 1024 * 3, 0x10);
    update_filled_chunk(&chunk_w3, 1024 * 1, 0x49);
    update_filled_chunk(&chunk_w1, 1024 * 2, 0x20);
    update_filled_chunk(&chunk_w3, 1024 * 3, 0x10);

    /* The area should have been recompacted now. Check that we can still read
     * our chunk */
    assert_chunk_latest_contents_match(&chunk_r2, 1024 * 2, 0x82);

    update_filled_chunk(&chunk_w2, 1024 * 1, 0x65);
    assert_chunk_latest_contents_match(&chunk_r2, 1024 * 1, 0x65);

    /* write a few more uninteresting chunks */
    update_filled_chunk(&chunk_w3, 1024 * 1, 0x49);
    update_filled_chunk(&chunk_w1, 1024 * 2, 0x20);
    update_filled_chunk(&chunk_w3, 1024 * 3, 0x10);
    update_filled_chunk(&chunk_w3, 1024 * 1, 0x49);
    update_filled_chunk(&chunk_w1, 1024 * 2, 0x20);
    update_filled_chunk(&chunk_w3, 1024 * 3, 0x10);
    update_filled_chunk(&chunk_w3, 1024 * 1, 0x49);
    update_filled_chunk(&chunk_w1, 1024 * 2, 0x20);
    update_filled_chunk(&chunk_w3, 1024 * 3, 0x10);

    /* update our chunk */
    update_filled_chunk(&chunk_w2, 1024 * 3, 0x11);

    /* write a few more uninteresting chunks */
    update_filled_chunk(&chunk_w3, 1024 * 1, 0x49);
    update_filled_chunk(&chunk_w1, 1024 * 2, 0x20);
    update_filled_chunk(&chunk_w3, 1024 * 3, 0x10);

    /* check that our chunk has the latest data */
    assert_chunk_latest_contents_match(&chunk_r2, 1024 * 3, 0x11);

    speculo_area_close(area_r);

    speculo_area_destroy(area_w);
}
END_TEST

static int test_stream_readers_reader(const char *area_name)
{
    speculo_area *area = NULL;
    speculo_chunk chunk;
    int i;

    i = 0;
    do {
        usleep(100);

        area = speculo_area_open(area_name);
        i++;
    } while (area == NULL && i < 20000);

    if (area == NULL) {
        fprintf(stderr, "speculo_area_open failed, error (%d)\n", errno);
        return EXIT_FAILURE;
    }

    speculo_area_init_chunk(area, &chunk);
    for (i = 0; i < 50; i++) {
        void *read_data;
        size_t length, j, expected_length;
        int ret;
        char expected_data = (char)i;
        int n_retries;

        expected_length = 1024 * (1 + i % 13);

        n_retries = 0;
        do {
            if (n_retries > 0) usleep(1000);
            ret = speculo_chunk_get_next(&chunk);
            n_retries++;
        } while (ret != 0 && n_retries < 100);
        //ck_assert(ret == 0);
        if (ret != 0) {
            fprintf(stderr, "speculo_chunk_get_next returned %d\n", ret);
            return EXIT_FAILURE;
        }

        ret = speculo_chunk_read(&chunk, &read_data, &length);
        //ck_assert(ret == 0);
        if (ret != 0) {
            fprintf(stderr, "speculo_chunk_read returned %d\n", ret);
            return EXIT_FAILURE;
        }

        //ck_assert_int_eq(length, expected_length);
        if (length != expected_length) {
            fprintf(stderr, "Length: %zd, expected: %zd (loop %d)\n", length, expected_length, i);
            return EXIT_FAILURE;
        }
        for (j = 0; j < length; j++) {
            if (((char *)read_data)[j] != expected_data) break;
        }
        if (j != length) {
            fprintf(stderr, "Data differ at offset %zd\n", j);
            return EXIT_FAILURE;
        }
        //ck_assert_msg(j == length, "Data differs at offset %d", j);
        speculo_chunk_read_done(&chunk);
    }
    speculo_area_close(area);
    return EXIT_SUCCESS;
}

static speculo_area *test_stream_readers_writer(const char *area_name)
{
    speculo_area *area;
    int i, ret;

    area = speculo_area_new(area_name, 0);
    ck_assert(area != NULL);

    ret = speculo_area_create(area);
    ck_assert_int_eq(ret, 0);

    for (i = 0; i < 50; i++) {
        speculo_chunk chunk;
        int ret;
        char *data_ptr;
        size_t length;

        ret = speculo_area_create_chunk(area, &chunk);
        ck_assert(ret == 0);

        length = 1024 * (1 + i % 13);
        data_ptr = speculo_chunk_allocate(&chunk, length);
        ck_assert_msg(data_ptr != NULL, "iteration %d, size %zd", i, length);

        memset(data_ptr, i, length);
        speculo_chunk_commit(&chunk);
    }

    return area;
}

START_TEST(test_stream_readers)
{
    const char area_name[] = "speculo-test-stream-readers";
    speculo_area *writer_area;
    pid_t children_pids[100];
    int i;

    /* Make sure that the area is clear */
    writer_area = test_stream_readers_writer(area_name);
    speculo_area_destroy(writer_area);

    for (i = 0; i < sizeof(children_pids) / sizeof(pid_t); i++) {
        pid_t pid = check_fork();
        ck_assert(pid >= 0);
        if (pid == 0) {
            /* child process: start reading */
            int ret = test_stream_readers_reader(area_name);
            exit(ret);
        }

        children_pids[i] = pid;
    }

    writer_area = test_stream_readers_writer(area_name);

    /* Wait for the reader process to terminate */
    for (i = 0; i < sizeof(children_pids) / sizeof(pid_t); i++) {
        int status = 0;
        waitpid(children_pids[i], &status, 0);
        ck_assert_msg(WIFEXITED(status), "Children %d exited with error!", i);
        ck_assert_msg(WEXITSTATUS(status) == EXIT_SUCCESS,
                      "Children %d exited with error!", i);
    }

    speculo_area_destroy(writer_area);
}
END_TEST

static int test_expiring_stream_reader(const char *area_name)
{
    speculo_area *area = NULL;
    speculo_chunk chunk;
    struct timespec start_time;
    struct timespec end_time;
#ifdef _POSIX_CPUTIME
    struct timespec start_time_p;
    struct timespec end_time_p;
#endif
    struct timespec open_time;
    struct timespec last_iter_time;
    unsigned int opening_time;
    unsigned char last_data = 0;
    int i;

    clock_gettime(CLOCK_MONOTONIC, &start_time);
#ifdef _POSIX_CPUTIME
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_time_p);
#endif

    i = 0;
    do {
        if (i > 0) usleep(100);
        area = speculo_area_open(area_name);
        i++;
    } while (area == NULL && i < 20000);

    clock_gettime(CLOCK_MONOTONIC, &open_time);
    opening_time = timespec_diff_ms(&open_time, &start_time);

    if (area == NULL) {
        fprintf(stderr, "speculo_area_open failed (after %u ms), error (%d)\n",
                opening_time, errno);
        return EXIT_FAILURE;
    }

    i = 0;
    last_iter_time = open_time;
    speculo_area_init_chunk(area, &chunk);
    do {
        void *read_data;
        size_t length, j;
        int ret;
        unsigned char expected_data;
        struct timespec iter_time;

        clock_gettime(CLOCK_MONOTONIC, &iter_time);

        ret = speculo_chunk_get_next(&chunk);
        if (ret != 0) {
            unsigned int wait_time;

            sched_yield();
            wait_time = timespec_diff_ms(&iter_time, &last_iter_time);
            if (wait_time > 4000) {
                fprintf(stderr, "speculo_chunk_get_next failed for %u ms\n",
                        wait_time);
                return EXIT_FAILURE;
            }
            continue;
        }

        ret = speculo_chunk_read(&chunk, &read_data, &length);
        if (ret != 0) {
            fprintf(stderr, "speculo_chunk_read returned %d\n", ret);
            return EXIT_FAILURE;
        }

        if (length % 1024 != 0) {
            fprintf(stderr, "unexpected length of chunk: %zd\n", length);
            return EXIT_FAILURE;
        }

        /* All bytes in the chunk must be the same */
        expected_data = ((unsigned char *)read_data)[0];
        for (j = 0; j < length; j++) {
            if (((unsigned char *)read_data)[j] != expected_data) break;
        }
        if (j != length) {
            fprintf(stderr, "Data differ at offset %zd\n", j);
            return EXIT_FAILURE;
        }

        /* and the data must be monotonically growing */
        if (expected_data <= last_data) {
            fprintf(stderr, "Data not growing: last was %hhx, now %hhx\n",
                    last_data, expected_data);
            return EXIT_FAILURE;
        }

        last_data = expected_data;
        speculo_chunk_read_done(&chunk);
        i++;
    } while (last_data != 0xab);
    speculo_area_close(area);

    clock_gettime(CLOCK_MONOTONIC, &end_time);
#ifdef _POSIX_CPUTIME
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end_time_p);
#endif
    fprintf(stdout, "Time %u"
#ifdef _POSIX_CPUTIME
            " (%u)"
#endif
            ", %d chunks\n",
            timespec_diff_ms(&end_time, &start_time),
#ifdef _POSIX_CPUTIME
            timespec_diff_ms(&end_time_p, &start_time_p),
#endif
            i);
    return EXIT_SUCCESS;
}

static speculo_area *test_expiring_stream_writer(const char *area_name)
{
    speculo_area *area;
    int i, ret;

    area = speculo_area_new(area_name, 0);
    ck_assert(area != NULL);

    speculo_area_set_max_size(area, 1024 * 64);

    ret = speculo_area_create(area);
    ck_assert_int_eq(ret, 0);

    for (i = 0; i < 100; i++) {
        speculo_chunk chunk;
        int ret;
        char *data_ptr;
        size_t length;

        ret = speculo_area_create_chunk(area, &chunk);
        ck_assert(ret == 0);

        length = 1024 * (1 + i % 10);
        data_ptr = speculo_chunk_allocate(&chunk, length);
        ck_assert_msg(data_ptr != NULL, "iteration %d, size %zd", i, length);

        speculo_chunk_set_expiration_time(&chunk, 5);

        memset(data_ptr, i + 1, length);
        speculo_chunk_commit(&chunk);

        /* this wait ensures that we give enough time to the chunks to expire,
         * and also to the readers not to miss too many chunks */
        usleep((i % 20) * 1000);
    }

    /* Write one last chunk, non-expiring, to mark the end */
    {
        speculo_chunk chunk;
        int ret;
        char *data_ptr;
        size_t length;

        ret = speculo_area_create_chunk(area, &chunk);
        ck_assert(ret == 0);

        length = 1024 * 10;
        data_ptr = speculo_chunk_allocate(&chunk, length);
        ck_assert_msg(data_ptr != NULL, "last iteration, size %zd", length);
        memset(data_ptr, 0xab, length);
        speculo_chunk_commit(&chunk);
    }

    return area;
}

START_TEST(test_expiring_stream)
{
    const char area_name[] = "speculo-test-expiring-stream";
    speculo_area *writer_area;
    pid_t children_pids[100];
    int i;

    /* Make sure that the area is clear */
    writer_area = test_stream_readers_writer(area_name);
    speculo_area_destroy(writer_area);

    for (i = 0; i < sizeof(children_pids) / sizeof(pid_t); i++) {
        pid_t pid = check_fork();
        ck_assert(pid >= 0);
        if (pid == 0) {
            /* child process: start reading */
            int ret = test_expiring_stream_reader(area_name);
            exit(ret);
        }

        children_pids[i] = pid;
    }

    writer_area = test_expiring_stream_writer(area_name);

    /* Wait for the reader process to terminate */
    for (i = 0; i < sizeof(children_pids) / sizeof(pid_t); i++) {
        int status = 0;
        waitpid(children_pids[i], &status, 0);
        ck_assert_msg(WIFEXITED(status), "Children %d exited with error!", i);
        ck_assert_msg(WEXITSTATUS(status) == EXIT_SUCCESS,
                      "Children %d exited with error!", i);
    }

    speculo_area_destroy(writer_area);
}
END_TEST

static int test_continuous_update_reader(const char *area_name, int base_size)
{
    speculo_area *area = NULL;
    speculo_chunk chunk;
    struct timespec start_time;
    struct timespec end_time;
#ifdef _POSIX_CPUTIME
    struct timespec start_time_p;
    struct timespec end_time_p;
#endif
    struct timespec open_time;
    unsigned int opening_time;
    void *read_data;
    size_t length;
    unsigned char content_byte;
    int i, ret;

    clock_gettime(CLOCK_MONOTONIC, &start_time);
#ifdef _POSIX_CPUTIME
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_time_p);
#endif

    i = 0;
    do {
        if (i > 0) usleep(100);
        area = speculo_area_open(area_name);
        i++;
    } while (area == NULL && i < 20000);

    clock_gettime(CLOCK_MONOTONIC, &open_time);
    opening_time = timespec_diff_ms(&open_time, &start_time);

    if (area == NULL) {
        fprintf(stderr, "speculo_area_open failed (after %u ms), error (%d)\n",
                opening_time, errno);
        return EXIT_FAILURE;
    }

    i = 0;
    speculo_area_init_chunk(area, &chunk);
    do {
        struct timespec iter_time;
        unsigned int wait_time;

        clock_gettime(CLOCK_MONOTONIC, &iter_time);

        ret = speculo_chunk_get_next(&chunk);
        if (ret == 0) break;

        sched_yield();
        wait_time = timespec_diff_ms(&iter_time, &open_time);
        if (wait_time > 4000) {
            fprintf(stderr, "speculo_chunk_get_next failed for %u ms\n",
                    wait_time);
            return EXIT_FAILURE;
        }
    } while (ret != 0);

    /* At this point we know that there are some valid chunks in the SHM area
     */
    speculo_area_init_chunk(area, &chunk);
    content_byte = 0xff;
    do {
        ret = speculo_chunk_get_next(&chunk);
        if (ret != 0) {
            fprintf(stderr, "speculo_chunk_read latest returned %d\n", ret);
            return EXIT_FAILURE;
        }

        speculo_chunk latest = chunk;
        ret = speculo_chunk_read_latest(&latest, &read_data, &length);
        if (ret < 0) {
            continue;
        }
        content_byte = ((unsigned char *)read_data)[0];
        speculo_chunk_read_done(&latest);
    } while (content_byte == 0xff);

    assert(content_byte != 0xff);
    /* we got our chunk. Continuosly read it, verifying that the data we read
     * is consistent (length = content_byte * base_size) */
    i = 0;
    do {
        size_t j;

        ret = speculo_chunk_read_latest(&chunk, &read_data, &length);
        if (ret < 0) {
            fprintf(stderr, "speculo_chunk_read_latest returned %d at %d\n", ret, i);
            return EXIT_FAILURE;
        }
        if (length % base_size != 0) {
            fprintf(stderr, "unexpected length of chunk: %zd\n", length);
            return EXIT_FAILURE;
        }

        content_byte = length / base_size;
        /* All bytes in the chunk must be the same */
        for (j = 0; j < length; j++) {
            if (((unsigned char *)read_data)[j] != content_byte) break;
        }
        if (j != length) {
            fprintf(stderr, "Data differ at offset %zd\n", j);
            return EXIT_FAILURE;
        }

        speculo_chunk_read_done(&chunk);
        i++;
        if (i % 5 == 0) sched_yield();
    } while (content_byte != 70);
    speculo_area_close(area);

    clock_gettime(CLOCK_MONOTONIC, &end_time);
#ifdef _POSIX_CPUTIME
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end_time_p);
#endif
    fprintf(stdout, "Time %u"
#ifdef _POSIX_CPUTIME
            " (%u)"
#endif
            ", %d updates\n",
            timespec_diff_ms(&end_time, &start_time),
#ifdef _POSIX_CPUTIME
            timespec_diff_ms(&end_time_p, &start_time_p),
#endif
            i);
    return EXIT_SUCCESS;
}

static speculo_area *test_continuous_update_writer(const char *area_name,
                                                   int base_size)
{
    speculo_area *area;
    speculo_chunk update_chunk;
    char *data_ptr;
    size_t length;
    int ret, i, n;

    area = speculo_area_new(area_name, 0);
    ck_assert(area != NULL);

    speculo_area_set_max_size(area, 1024 * 16);

    ret = speculo_area_create(area);
    ck_assert_int_eq(ret, 0);

    ret = speculo_area_create_chunk(area, &update_chunk);
    ck_assert(ret == 0);

    length = base_size * 1;
    data_ptr = speculo_chunk_allocate(&update_chunk, length);
    ck_assert_msg(data_ptr != NULL);

    memset(data_ptr, 1, length);
    speculo_chunk_commit(&update_chunk);

    for (i = 0; i < 400; i++) {
        /* From time to time create also another chunk which the readers are
         * not interested in, just to add some spice to the test */
        if (i % 9 == 0) {
            speculo_chunk chunk;

            ret = speculo_area_create_chunk(area, &chunk);
            ck_assert(ret == 0);

            length = base_size * (1 + i % 3);
            data_ptr = speculo_chunk_allocate(&chunk, length);
            ck_assert_msg(data_ptr != NULL, "iteration %d, size %zd", i, length);

            speculo_chunk_set_expiration_time(&chunk, 5);

            memset(data_ptr, 0xff, length);
            speculo_chunk_commit(&chunk);
        }

        n = 1 + i % 31;
        length = base_size * n;
        data_ptr = speculo_chunk_update(&update_chunk, length);
        ck_assert_msg(data_ptr != NULL, "iteration %d, size %zd", i, length);

        memset(data_ptr, n, length);
        speculo_chunk_commit(&update_chunk);
        if (i % 17 == 0) sched_yield();
    }

    /* write a sentinel chunk, to let the readers know that we are done */
    n = 70;
    length = base_size * n;
    data_ptr = speculo_chunk_update(&update_chunk, length);
    ck_assert(data_ptr != NULL);

    memset(data_ptr, n, length);
    speculo_chunk_commit(&update_chunk);

    return area;
}

START_TEST(test_continuous_update)
{
    const char area_name[] = "speculo-test-continuous-update";
    speculo_area *writer_area;
    pid_t children_pids[100];
    int base_size = 128;
    int i;

    for (i = 0; i < sizeof(children_pids) / sizeof(pid_t); i++) {
        pid_t pid = check_fork();
        ck_assert(pid >= 0);
        if (pid == 0) {
            /* child process: start reading */
            int ret = test_continuous_update_reader(area_name, base_size);
            exit(ret);
        }

        children_pids[i] = pid;
    }

    writer_area = test_continuous_update_writer(area_name, base_size);

    /* Wait for the reader process to terminate */
    for (i = 0; i < sizeof(children_pids) / sizeof(pid_t); i++) {
        int status = 0;
        waitpid(children_pids[i], &status, 0);
        ck_assert_msg(WIFEXITED(status), "Children %d exited with error!", i);
        ck_assert_msg(WEXITSTATUS(status) == EXIT_SUCCESS,
                      "Children %d exited with error!", i);
    }

    speculo_area_destroy(writer_area);
}
END_TEST

Suite *speculo_suite(const char *test_cases)
{
    Suite *s;
    TCase *tc;

#define IF_TEST_CASE_ENABLED(test_name) \
    if (test_cases == NULL || strstr (test_cases, test_name) != NULL)

    s = suite_create("speculo");
    tc = tcase_create("core");
    tcase_add_test(tc, test_reader_open_unexisting);
    tcase_add_test(tc, test_open);
    tcase_add_test(tc, test_write_and_read);
    tcase_add_test(tc, test_write_and_read_compact);
    tcase_add_test(tc, test_compact_between_reads);
    tcase_add_test(tc, test_compact_between_get_next_and_read);
    tcase_add_test(tc, test_compact_before_read_done);
    tcase_add_test(tc, test_read_latest);
    IF_TEST_CASE_ENABLED("core")
        suite_add_tcase(s, tc);

    tc = tcase_create("multiprocess");
    tcase_set_timeout(tc, 30);
    tcase_add_test(tc, test_stream_readers);
    tcase_add_test(tc, test_expiring_stream);
    tcase_add_test(tc, test_continuous_update);
    IF_TEST_CASE_ENABLED("multiprocess")
        suite_add_tcase(s, tc);

    return s;
}

int main(int argc, char **argv)
{
    const char *test_cases;
    int n_failed = 0;

    test_cases = getenv ("TEST_CASES");

    Suite *s = speculo_suite(test_cases);
    SRunner *sr = srunner_create(s);
    srunner_run_all(sr, CK_NORMAL);
    n_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return n_failed == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
